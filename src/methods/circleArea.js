// circleArea.js
/**
 * Calculate circle area
 * @param {*} raduis
 * @returns {number} area
 */
function getCircleArea(raduis) {
  return Math.PI * raduis * raduis;
}

module.exports = { getCircleArea };
