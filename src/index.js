const { getCircleArea } = require("./methods/circleArea.js");
const { getRectangleArea } = require("./methods/rectangleArea.js");
const { getTriangleArea } = require("./methods/triangleArea.js");

module.exports = {
  getCircleArea,
  getRectangleArea,
  getTriangleArea,
};
