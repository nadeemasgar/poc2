// circleArea.test.js
const { getCircleArea } = require("../src/index.js");

test("test if getCircleArea return a truthy value", () => {
  expect(getCircleArea(1)).toBeTruthy();
});

test("Calculate area of circle with rayon = 1, result expected : 1*1*pi = pi", () => {
  expect(getCircleArea(1)).toBe(Math.PI);
});
// rectangleArea.test.js
const { getRectangleArea } = require("../src/index.js");

test("test if getRectangleArea return a truthy value", () => {
  expect(getRectangleArea(1, 1)).toBeTruthy();
});

test("Calculate area of rectangle with b = 2 h = 2, result expected : 2*2 = 4", () => {
  expect(getRectangleArea(2, 2)).toBe(4);
});
// triangle.test.js
const { getTriangleArea } = require("../src/index.js");

test("test if getTriangleArea return a truthy value", () => {
  expect(getTriangleArea(1, 1)).toBeTruthy();
});

test("Calculate area of triangle with b = 1 h = 2, result expected : 1*2*0.5 = 1", () => {
  expect(getTriangleArea(1, 2)).toBe(1);
});
